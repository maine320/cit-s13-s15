package com.example.discussion.exceptions;

public class UserException extends Exception{

        public UserException(String message){
            //"Super()" can be used to invoke immediate parent class constructor
            super(message);
        }
    }

